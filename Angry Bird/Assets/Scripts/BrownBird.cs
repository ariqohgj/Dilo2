﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrownBird : Bird
{
    [SerializeField]
    public float _radius = 3;
   public CircleCollider2D ccolider;

    void Update()
    {
        if( State == BirdState.HitSomething)
        {
            StartCoroutine(meledak());
        }
    }

    IEnumerator meledak()
    {
        yield return new WaitForSeconds(2);
        ccolider.radius = _radius;
    }
    public override void OnTap()
    {
        
    }
}
