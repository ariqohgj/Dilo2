﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    public Image helth;
    public Image speedup;

    Text text;


    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;
    }


    void Update ()
    {
        text.text = "Score: " + score;

        if(score > 240)
        {
            helth.color = new Color(1, 1, 1, 1);
        }
        else
        {
            helth.color = new Color(1, 1, 1, 0);
        }

        if (score > 50)
        {
            speedup.color = new Color(1, 1, 1, 1);
        }
        else
        {
            speedup.color = new Color(1, 1, 1, 0);
        }
    }
}
